<div align='center'>

<img src='./assets/screenshots/mini_tasker_cover.jpg' height=128> 

# MiniTasker

> Streamline your tasks effortlessly with our Minitasker. Organize, categorize, and manage your to-dos with ease.
> Made with ❤ on React Native.

<img src='./assets/screenshots/1.png' height=420> <img src='./assets/screenshots/2.png' height=420> <img src='./assets/screenshots/3.png' height=420>
<img src='./assets/screenshots/1.png' height=420> <img src='./assets/screenshots/4.png' height=420> <img src='./assets/screenshots/5.png' height=420>
<img src='./assets/screenshots/6.png' height=420> <img src='./assets/screenshots/7.png' height=420> <img src='./assets/screenshots/8.png' height=420>
<img src='./assets/screenshots/9.png' height=420> <img src='./assets/screenshots/10.png' height=420> <img src='./assets/screenshots/11.png' height=420>

<img src='./assets/screenshots/recording.mp4' height=420>
<!-- Hello -->
</div>
